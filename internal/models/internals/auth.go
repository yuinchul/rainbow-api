package internals

type AuthTokenPayload struct {
	UserID string
	Role   string
}
