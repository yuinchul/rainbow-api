package internals

type Account struct {
	ID        int
	Email     string
	Password  string
	Verified  bool
	Suspended bool
	ToDelete  bool
	Rules     []int
}

type Rule struct {
	ID   int
	Type string
}
