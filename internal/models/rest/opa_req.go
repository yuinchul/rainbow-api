package rest

type OpaReq struct {
	UserID string `json:"user_id"`
	Method string `json:"method"`
	Path   string `json:"path"`
	Role   string `json:"role"`
}

type OpaInputReq struct {
	Input OpaReq
}
