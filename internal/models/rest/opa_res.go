package rest

type OpaAllow struct {
	Allow bool `json:"allow"`
}

type OpaResult struct {
	Result OpaAllow `json:"result"`
}
