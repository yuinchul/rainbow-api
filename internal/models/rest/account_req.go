package rest

// New User Request
// swagger:model AccountRegistrationReq
type AccountRegistrationReq struct {
	// Email address
	// unique: true
	// required: true
	// example: dummy@dummy.com
	Email string `json:"email" validate:"required,email"`
	// Password
	// Min Length: 10
	// required: true
	// example: dummy12345678
	Password string `json:"password" validate:"required,gte=10"`
	// Password confirmation
	// Min Length: 10
	// required: true
	// example: dummy12345678
	ConfirmPassword string `json:"confirm_password" validate:"required,gte=10"`
}

// Login Request
// swagger:model LoginReq
type LoginReq struct {
	// Email Address
	// unique: true
	// required: true
	// example: dummy@dummy.com
	Email string `json:"email" validate:"required,email"`
	// Password
	// Min Length: 10
	// required: true
	// example: dummy12345678
	Password string `json:"password" validate:"required,gte=10"`
}

// Email Verification Code Request
// swagger:model CodeReq
type CodeReq struct {
	// Email address
	// unique: true
	// required: true
	// example: dummy@dummy.com
	Email string `json:"email" validate:"required,email"`
}

// Email Verification Request
// swagger:model EmailValidationReq
type EmailValidationReq struct {
	// Code got by email
	// required: true
	// example: 123e4567-e89b-12d3-a456-426614174000
	ID string `json:"id" validate:"required"`
}

// Update Password
// swagger:model UpdatePassReq
type UpdatePassReq struct {
	// Password
	// Min Length: 10
	// required: true
	// example: dummy12345678
	Password string `json:"password" validate:"required,gte=10"`
	// New Password
	// Min Length: 10
	// required: true
	// example: newdummy12345678
	NewPassword string `json:"new_password" validate:"required,gte=10"`
	// New Password confirmation
	// required: true
	// example: newdummy12345678
	ConfirmPassword string `json:"confirm_password" validate:"required,gte=10"`
}

// Reset Password Request. Forgotten password
// swagger:model ResetPassReq
type ResetPassReq struct {
	// Code got by email
	// required: true
	// example: 123e4567-e89b-12d3-a456-426614174000
	ID string `json:"id" validate:"required"`
	// Email address
	// unique: true
	// required: true
	// example: dummy@dummy.com
	Email string `json:"email" validate:"required,email"`
	UpdatePassReq
}

// Request to Delete/Restore/Suspend/Unsuspend an account
// swagger:model AccountChangeStatusReq
type AccountChangeStatusReq struct {
	// Email address
	// unique: true
	// required: true
	// example: dummy@dummy.com
	Email string `json:"email" validate:"required,email"`
	// Password
	// Min Length: 10
	// required: true
	// example: dummy12345678
	Password string `json:"password" validate:"required,gte=10"`
}
