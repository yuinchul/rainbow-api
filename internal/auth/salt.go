package auth

import "golang.org/x/crypto/bcrypt"

type SaltHandler interface {
	ComparePassword(string, string) error
	SaltPassword(string) (string, error)
}

type saltBcrypt struct{}

func NewSaltHandler() SaltHandler {
	return &saltBcrypt{}
}

func (s *saltBcrypt) ComparePassword(passwordDB string,
	passwordBody string) error {
	saltErr := bcrypt.CompareHashAndPassword([]byte(passwordDB),
		[]byte(passwordBody))
	return saltErr
}

func (s *saltBcrypt) SaltPassword(password string) (string, error) {
	sPass, sPassError := bcrypt.GenerateFromPassword(
		[]byte(password),
		bcrypt.DefaultCost)
	if sPassError != nil {
		return "", sPassError
	}
	pass := string(sPass)
	return pass, nil
}
