package services

import (
	"database/sql"
	"gitlab.com/rainbowproject/rainbow-api/internal/database/doa"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
	"strings"

	"gitlab.com/rainbowproject/rainbow-api/internal/auth"
	"gitlab.com/rainbowproject/rainbow-api/internal/mailer"
	"gitlab.com/rainbowproject/rainbow-api/internal/messages"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/internals"
	"gitlab.com/rainbowproject/rainbow-api/internal/redis"
	"gitlab.com/rainbowproject/rainbow-api/internal/validator"

	"github.com/rs/zerolog/log"
)

type AccountServiceHandler interface {
	Create(rest.AccountRegistrationReq) (rest.Suc, rest.Err)
	Login(rest.LoginReq) (rest.Suc, rest.Err)
	Suspend(rest.AccountChangeStatusReq) (rest.Suc, rest.Err)
	Unsuspend(rest.AccountChangeStatusReq) (rest.Suc, rest.Err)
	Delete(rest.AccountChangeStatusReq) (rest.Suc, rest.Err)
	Restore(rest.AccountChangeStatusReq) (rest.Suc, rest.Err)
	NewCodeEmail(rest.CodeReq) (rest.Suc, rest.Err)
	ValidateEmail(rest.EmailValidationReq) (rest.Suc, rest.Err)
	NewCodeResetPass(rest.CodeReq) (rest.Suc, rest.Err)
	ResetPassword(rest.ResetPassReq) (rest.Suc, rest.Err)
	UpdatePassword(string, rest.UpdatePassReq) (rest.Suc, rest.Err)
}

type account struct {
	rEmail    redis.ClientHandler
	rPass     redis.ClientHandler
	email     mailer.EmailHandler
	dbService doa.DAOAccount
	v         validator.ValidateJSON
	jwt       auth.TokenHandler
}

func NewAccountServiceHandler(db *sql.DB,
	v validator.ValidateJSON) *account {
	return &account{
		rEmail:    redis.NewRedisEmailHandler(),
		rPass:     redis.NewRedisPassHandler(),
		dbService: doa.NewDOAccountHandler(db),
		email:     mailer.NewEmailHandler(),
		v:         v,
		jwt:       auth.NewTokenHandler(),
	}
}

func (s *account) Create(r rest.AccountRegistrationReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Create a new account")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	if r.Password != r.ConfirmPassword {
		log.Error().Msg(messages.AccountPassNotMatch)
		e := rest.NewBadRequestError(messages.AccountPassNotMatch)
		return nil, e
	}
	r.Email = strings.ToLower(r.Email)
	dbU, _ := s.dbService.FetchByEmail(r.Email)
	if dbU != nil {
		log.Error().Msg(messages.AccountAlreadyExists)
		e := rest.NewBadRequestError(messages.AccountAlreadyExists)
		return nil, e
	}
	sPass, sErr := auth.NewSaltHandler().SaltPassword(r.Password)
	if sErr != nil {
		log.Error().Msg(sErr.Error())
		e := rest.NewInternalServerError(messages.AuthSaltFail)
		return nil, e
	}
	pAccount := internals.Account{
		Email:    strings.ToLower(r.Email),
		Password: sPass,
		Verified: false,
	}
	cErr := s.dbService.Create(&pAccount)
	if cErr != nil {
		return nil, cErr
	}
	go s.email.SendUUIDEmailValidation(s.rEmail, r.Email)
	suc := rest.NewSuccessCreated(messages.AccountCreated, nil)
	return suc, nil
}

func (s *account) Login(r rest.LoginReq) (rest.Suc,
	rest.Err) {
	log.Trace().Msg("SERVICE - Account login")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	r.Email = strings.ToLower(r.Email)
	uDB, fErr := s.dbService.FetchByEmail(r.Email)
	if fErr != nil {
		return nil, fErr
	}
	if uDB.ToDelete {
		log.Debug().Msg(messages.AccountToDelete)
		e := rest.NewBadRequestError(messages.AccountToDelete)
		return nil, e
	}
	if uDB.Suspended {
		log.Debug().Msg(messages.AccountSuspended)
		e := rest.NewBadRequestError(messages.AccountSuspended)
		return nil, e
	}
	if !uDB.Verified {
		log.Error().Msg(messages.AccountEmailNotValidated)
		e := rest.NewBadRequestError(messages.AccountEmailNotValidated)
		return nil, e
	}
	cErr := auth.NewSaltHandler().ComparePassword(uDB.Password, r.Password)
	if cErr != nil {
		log.Error().Msg(cErr.Error())
		e := rest.NewBadRequestError(messages.AccountInvalidPassword)
		return nil, e
	}
	r.Email = strings.ToLower(r.Email)
	authPayload := internals.AuthTokenPayload{
		UserID: r.Email,
		Role:   "user",
	}
	jwtK, jwtKErr := s.jwt.Create(authPayload)
	if jwtKErr != nil {
		log.Error().Msg(jwtKErr.Error())
		e := rest.NewInternalServerError(messages.AuthGenTokenFail)
		return nil, e
	}
	token := rest.TokenResp{
		BearerToken: jwtK,
	}
	suc := rest.NewBasicSuccess(messages.AuthSuccess, token)
	return suc, nil
}

func (s *account) Suspend(r rest.AccountChangeStatusReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msgf("SERVICE - Suspending account")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	r.Email = strings.ToLower(r.Email)
	uDB, fErr := s.dbService.FetchByEmail(r.Email)
	if fErr != nil {
		return nil, fErr
	}
	if uDB.ToDelete {
		log.Debug().Msg(messages.AccountToDelete)
		e := rest.NewBadRequestError(messages.AccountToDelete)
		return nil, e
	}
	if !uDB.Suspended {
		cErr := auth.NewSaltHandler().ComparePassword(uDB.Password, r.Password)
		if cErr != nil {
			log.Error().Msg(cErr.Error())
			e := rest.NewBadRequestError(
				messages.AccountInvalidPassword)
			return nil, e
		}
		dErr := s.dbService.Suspend(r.Email, true)
		if dErr != nil {
			return nil, dErr
		}
	}
	suc := rest.NewBasicSuccess(messages.AccountSuspended, nil)
	return suc, nil
}

func (s *account) Unsuspend(r rest.AccountChangeStatusReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msgf("SERVICE - Unsuspending account")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	r.Email = strings.ToLower(r.Email)
	uDB, fErr := s.dbService.FetchByEmail(r.Email)
	if fErr != nil {
		return nil, fErr
	}
	if uDB.ToDelete {
		log.Debug().Msg(messages.AccountToDelete)
		e := rest.NewBadRequestError(messages.AccountToDelete)
		return nil, e
	}
	if uDB.Suspended {
		cErr := auth.NewSaltHandler().ComparePassword(uDB.Password, r.Password)
		if cErr != nil {
			log.Error().Msg(cErr.Error())
			e := rest.NewBadRequestError(
				messages.AccountInvalidPassword)
			return nil, e
		}
		dErr := s.dbService.Suspend(r.Email, false)
		if dErr != nil {
			return nil, dErr
		}
	}
	suc := rest.NewBasicSuccess(messages.AccountUnsuspended, nil)
	return suc, nil
}

func (s *account) Delete(r rest.AccountChangeStatusReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msgf("SERVICE - Mark account to delete")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	r.Email = strings.ToLower(r.Email)
	uDB, fErr := s.dbService.FetchByEmail(r.Email)
	if fErr != nil {
		return nil, fErr
	}
	if !uDB.ToDelete {
		cErr := auth.NewSaltHandler().ComparePassword(uDB.Password, r.Password)
		if cErr != nil {
			log.Error().Msg(cErr.Error())
			e := rest.NewBadRequestError(
				messages.AccountInvalidPassword)
			return nil, e
		}
		dErr := s.dbService.Delete(r.Email, true)
		if dErr != nil {
			return nil, dErr
		}
	}
	suc := rest.NewBasicSuccess(messages.AccountToDelete, nil)
	return suc, nil
}

func (s *account) Restore(r rest.AccountChangeStatusReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msgf("SERVICE - Try to restore account")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	r.Email = strings.ToLower(r.Email)
	uDB, fErr := s.dbService.FetchByEmail(r.Email)
	if fErr != nil {
		return nil, fErr
	}
	if uDB.ToDelete {
		cErr := auth.NewSaltHandler().ComparePassword(uDB.Password, r.Password)
		if cErr != nil {
			log.Error().Msg(cErr.Error())
			e := rest.NewBadRequestError(
				messages.AccountInvalidPassword)
			return nil, e
		}
		dErr := s.dbService.Delete(r.Email, false)
		if dErr != nil {
			return nil, dErr
		}
	}
	suc := rest.NewBasicSuccess(messages.AccountRestored, nil)
	return suc, nil
}

func (s *account) NewCodeEmail(r rest.CodeReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Generate UUID to verify this account email")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	r.Email = strings.ToLower(r.Email)
	dbU, dErr := s.dbService.FetchByEmail(r.Email)
	if dErr != nil {
		return nil, dErr
	}
	if dbU.Verified {
		log.Debug().Msg(messages.AccountEmailAlreadyValidated)
		return nil, rest.NewBadRequestError(messages.AccountEmailAlreadyValidated)
	}
	go s.email.SendUUIDEmailValidation(s.rEmail, r.Email)
	suc := rest.NewSuccessCreated(messages.AccountTokenGenerated, nil)
	return suc, nil
}

func (s *account) ValidateEmail(r rest.EmailValidationReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Validate Email by UUID")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	id := strings.Trim(r.ID, " ")
	email, rErr := s.rEmail.Get(id)
	if rErr != nil {
		log.Error().Msg(rErr.Error())
		e := rest.NewNotFoundError(messages.RedisInvalidKey)
		return nil, e
	}
	uDB, uErr := s.dbService.FetchByEmail(email)
	if uErr != nil {
		return nil, uErr
	}
	if uDB.Verified {
		log.Debug().Msg(messages.AccountEmailAlreadyValidated)
		go s.rEmail.Del(id)
		e := rest.NewBadRequestError(
			messages.AccountEmailAlreadyValidated)
		return nil, e
	}
	upErr := s.dbService.SetEmailAsValid(email)
	if upErr != nil {
		return nil, upErr
	}
	go s.rEmail.Del(id)
	suc := rest.NewBasicSuccess(messages.AccountEmailValidated, nil)
	return suc, nil
}

func (s *account) NewCodeResetPass(r rest.CodeReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Generate UUID to reset this account password")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	r.Email = strings.ToLower(r.Email)
	_, uErr := s.dbService.FetchByEmail(r.Email)
	if uErr != nil {
		return nil, uErr
	}

	go s.email.SendUUIDPassReset(s.rPass, r.Email)
	suc := rest.NewSuccessCreated(messages.AccountTokenGenerated, nil)
	return suc, nil
}

func (s *account) ResetPassword(r rest.ResetPassReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Reset account Password")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	if r.NewPassword != r.ConfirmPassword {
		log.Error().Msg(messages.AccountPassNotMatch)
		e := rest.NewBadRequestError(messages.AccountPassNotMatch)
		return nil, e
	}
	id := strings.Trim(r.ID, " ")
	email, rErr := s.rPass.Get(id)
	if rErr != nil {
		log.Error().Msg(rErr.Error())
		e := rest.NewNotFoundError(messages.RedisInvalidKey)
		return nil, e
	}
	uDB, uErr := s.dbService.FetchByEmail(email)
	if uErr != nil {
		return nil, uErr
	}
	cErr := auth.NewSaltHandler().ComparePassword(uDB.Password, r.Password)
	if cErr != nil {
		log.Error().Msg(cErr.Error())
		e := rest.NewBadRequestError(messages.AccountInvalidPassword)
		return nil, e
	}
	sPass, sErr := auth.NewSaltHandler().SaltPassword(r.NewPassword)
	if sErr != nil {
		log.Error().Msg(sErr.Error())
		e := rest.NewInternalServerError(messages.AuthSaltFail)
		return nil, e
	}
	e := s.dbService.UpdatePassword(r.Email, sPass)
	if e != nil {
		return nil, e
	}
	go s.rPass.Del(id)
	suc := rest.NewBasicSuccess(messages.AccountPassUpdated, nil)
	return suc, nil
}

func (s *account) UpdatePassword(email string, r rest.UpdatePassReq) (
	rest.Suc, rest.Err) {
	log.Trace().Msg("SERVICE - Update account Password")
	vErr := s.v.Validate(r)
	if vErr != nil {
		log.Error().Msg(vErr.Error())
		e := rest.NewBadRequestError(vErr.Error())
		return nil, e
	}
	if r.NewPassword != r.ConfirmPassword {
		log.Error().Msg(messages.AccountPassNotMatch)
		e := rest.NewBadRequestError(messages.AccountPassNotMatch)
		return nil, e
	}
	uDB, uErr := s.dbService.FetchByEmail(email)
	if uErr != nil {
		return nil, uErr
	}
	cErr := auth.NewSaltHandler().ComparePassword(uDB.Password, r.Password)
	if cErr != nil {
		log.Error().Msg(cErr.Error())
		e := rest.NewBadRequestError(messages.AccountInvalidPassword)
		return nil, e
	}
	sPass, sErr := auth.NewSaltHandler().SaltPassword(r.NewPassword)
	if sErr != nil {
		log.Error().Msg(sErr.Error())
		e := rest.NewInternalServerError(messages.AuthSaltFail)
		return nil, e
	}
	e := s.dbService.UpdatePassword(email, sPass)
	if e != nil {
		return nil, e
	}
	suc := rest.NewBasicSuccess(messages.AccountPassUpdated, nil)
	return suc, nil
}
