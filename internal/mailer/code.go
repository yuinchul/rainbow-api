package mailer

import (
	"github.com/google/uuid"
)

type uuidGen struct{}

type CodeHandler interface {
	NewCode() string
}

func NewVerificationCodeHandler() *uuidGen {
	return &uuidGen{}
}

func (e *uuidGen) NewCode() string {
	UUID := uuid.New()
	return UUID.String()
}
