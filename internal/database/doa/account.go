package doa

import (
	"database/sql"
	"github.com/rs/zerolog/log"
	"gitlab.com/rainbowproject/rainbow-api/internal/messages"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/internals"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
	"gitlab.com/rainbowproject/rainbow-api/internal/redis"
)

const (
	queryInsert           = "INSERT INTO users(email, verified, password,suspended,to_delete) VALUES(?,0,?,0,0)"
	queryFindByEmail      = "SELECT email, verified, password, suspended, to_delete FROM users WHERE email=?"
	queryUpdateVerify     = "UPDATE users SET verified=1 WHERE email=? AND to_delete=0 AND suspended=0"
	queryUpdatePass       = "UPDATE users SET password=? WHERE email=? AND to_delete=0 AND suspended=0"
	queryUpdateSuspension = "UPDATE users SET suspended=? WHERE email=? AND to_delete=0"
	queryUpdateDeletion   = "UPDATE users SET to_delete=? WHERE email=?"
)

type DAOAccount interface {
	Create(*internals.Account) rest.Err
	Delete(string, bool) rest.Err
	Suspend(string, bool) rest.Err
	FetchByEmail(string) (*internals.Account, rest.Err)
	UpdatePassword(string, string) rest.Err
	SetEmailAsValid(string) rest.Err
}

type mysqlAccount struct {
	rEmail redis.ClientHandler
	rPass  redis.ClientHandler
	db     *sql.DB
}

func NewDOAccountHandler(db *sql.DB) *mysqlAccount {
	return &mysqlAccount{
		rEmail: redis.NewRedisEmailHandler(),
		rPass:  redis.NewRedisPassHandler(),
		db:     db,
	}
}

func (d *mysqlAccount) UpdatePassword(email string,
	nPass string) rest.Err {
	log.Logger.Trace().Msg("DB - Update account password")
	stmt, pErr := d.db.Prepare(queryUpdatePass)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return e
	}
	defer stmt.Close()

	_, eErr := stmt.Exec(nPass, email)
	if eErr != nil {
		e := rest.NewInternalServerError(messages.SqlFailExecute)
		log.Error().Msg(eErr.Error())
		return e
	}
	return nil
}

func (d *mysqlAccount) SetEmailAsValid(email string) rest.Err {
	log.Trace().Msg("DB - Set account email as valid")
	stmt, pErr := d.db.Prepare(queryUpdateVerify)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return e
	}
	defer stmt.Close()
	_, eErr := stmt.Exec(email)
	if eErr != nil {
		e := rest.NewInternalServerError(messages.SqlFailExecute)
		log.Error().Msg(eErr.Error())
		return e
	}
	return nil
}

func (d *mysqlAccount) FetchByEmail(email string) (*internals.Account,
	rest.Err) {
	log.Trace().Msg("DB - Get account by email")
	stmt, pErr := d.db.Prepare(queryFindByEmail)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return nil, e
	}
	defer stmt.Close()
	var account internals.Account
	sErr := stmt.QueryRow(email).Scan(&account.Email, &account.Verified,
		&account.Password, &account.Suspended, &account.ToDelete)
	if sErr != nil {
		e := rest.NewInternalServerError(messages.SqlNotFound)
		log.Error().Msg(sErr.Error())
		return nil, e
	}
	return &account, nil
}

func (d *mysqlAccount) Create(account *internals.Account) rest.Err {
	log.Trace().Msg("DB - Create new account")
	stmt, pErr := d.db.Prepare(queryInsert)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return e
	}
	defer stmt.Close()

	_, eErr := stmt.Exec(account.Email, account.Password)
	if eErr != nil {
		e := rest.NewInternalServerError(messages.SqlFailExecute)
		log.Error().Msg(eErr.Error())
		return e
	}
	return nil
}

func (d *mysqlAccount) Delete(email string, deletion bool) rest.Err {
	log.Trace().Msgf("DB - Mark account to be deleted %t", deletion)
	stmt, pErr := d.db.Prepare(queryUpdateDeletion)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return e
	}
	defer stmt.Close()

	_, eErr := stmt.Exec(deletion, email)
	if eErr != nil {
		e := rest.NewInternalServerError(messages.SqlFailExecute)
		log.Error().Msg(eErr.Error())
		return e
	}
	return nil
}

func (d *mysqlAccount) Suspend(email string,
	suspended bool) rest.Err {
	log.Trace().Msgf("DB - Mark account to be suspended %t", suspended)
	stmt, pErr := d.db.Prepare(queryUpdateSuspension)
	if pErr != nil {
		e := rest.NewInternalServerError(messages.SqlPrepareFail)
		log.Error().Msg(pErr.Error())
		return e
	}
	defer stmt.Close()
	_, eErr := stmt.Exec(suspended, email)
	if eErr != nil {
		e := rest.NewInternalServerError(messages.SqlFailExecute)
		log.Error().Msg(eErr.Error())
		return e
	}
	return nil
}
