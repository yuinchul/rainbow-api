package database

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql" // MySQL driver
	"github.com/rs/zerolog/log"
	"gitlab.com/rainbowproject/rainbow-api/internal/config"
)

type DBHandler interface {
	NewDBConnection() *sql.DB
}

type mysqlDB struct{}

func NewDBHandler() *mysqlDB {
	return &mysqlDB{}
}

// NewNewDBConnection Get a new instance of DBHandler Interface
func (m *mysqlDB) NewDBConnection() *sql.DB {
	log.Trace().Msg("Initializing MysqlDB")

	cDB := config.GetDbEnvVars()
	connectionString := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8",
		cDB.Username, cDB.Password, cDB.Host, cDB.Schema)

	db, err := sql.Open("mysql", connectionString)
	if err != nil {
		log.Fatal().Msg(err.Error())
	}

	if db == nil {
		log.Fatal().Msg("Something went wrong when trying to connect to the DB")
	} else {
		err = db.Ping()
		if err != nil {
			log.Fatal().Msg(err.Error())
		}
	}

	log.Info().Msgf("Connected to %s:%s@tcp(%s)/%s?charset=utf8",
		cDB.Username, "****", cDB.Host, cDB.Schema)
	return db
}
