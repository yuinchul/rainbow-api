package logging

import (
	"github.com/rs/zerolog/log"
	"net/http"
)

func MuxMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Debug().Msgf("<- %s :: %s :: %s :: %s", r.Method, r.RequestURI,
			r.UserAgent(), r.Proto)
		next.ServeHTTP(w, r)
	})
}
