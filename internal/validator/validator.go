package validator

import (
	"github.com/go-playground/validator"
)

type ValidateJSON interface {
	Validate(i interface{}) error
}

type validateJSON struct {
	v *validator.Validate
}

func NewValidator() *validateJSON {
	return &validateJSON{
		v: validator.New(),
	}
}

func (v *validateJSON) Validate(i interface{}) error {
	return v.v.Struct(i)
}
