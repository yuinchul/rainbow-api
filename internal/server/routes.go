package server

import (
	"database/sql"
	"github.com/gorilla/mux"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/rainbowproject/rainbow-api/internal/auth"
	"gitlab.com/rainbowproject/rainbow-api/internal/controllers"
	"gitlab.com/rainbowproject/rainbow-api/internal/logging"
	"gitlab.com/rainbowproject/rainbow-api/internal/validator"
	"net/http"
)

const apiVersion = "/v1"

func SetupRouter(db *sql.DB) *mux.Router {
	v := validator.NewValidator()

	hC := controllers.NewHealthController()
	aC := controllers.NewAccountController(db, v)
	pC := controllers.NewProfileController(db, v)

	r := mux.NewRouter().StrictSlash(true)

	r.Use(logging.MuxMiddleware)

	openR := r.PathPrefix(apiVersion).Subrouter()
	authR := r.PathPrefix(apiVersion).Subrouter()

	authR.Use(auth.MuxMiddleware)

	/* Health Check */
	registerRouter(openR, "GET", "/health", hC.HealthGet)

	/* Account routes */
	registerRouter(openR, "POST", "/email/newcode", aC.NewEmailCode)
	registerRouter(openR, "PUT", "/email/validate", aC.ValidateEmail)

	registerRouter(openR, "POST", "/pass/newcode", aC.NewResetPassCode)
	registerRouter(openR, "PUT", "/pass/reset", aC.ResetPass)

	registerRouter(authR, "PUT", "/account/pass/update", aC.UpdatePass)
	registerRouter(openR, "POST", "/account/signin", aC.SignIn)
	registerRouter(openR, "PUT", "/account/login", aC.Login)

	registerRouter(openR, "PUT", "/account/suspend", aC.Suspend)
	registerRouter(openR, "PUT", "/account/unsuspend", aC.Unsuspend)

	registerRouter(openR, "PUT", "/account/restore", aC.Restore)
	registerRouter(openR, "PUT", "/account/delete", aC.Delete)

	/* Profile routes */
	registerRouter(authR, "GET", "/profile", pC.FetchProfile)
	registerRouter(authR, "DELETE", "/profile", pC.Delete)

	registerRouter(authR, "PATCH", "/profile/info", pC.UpdateProfile)
	registerRouter(authR, "PATCH", "/profile/avatar", pC.UpdateAvatar)

	if log.Logger.GetLevel() == zerolog.TraceLevel {
		_ = r.Walk(printRoutes)
	}

	return r
}

func registerRouter(r *mux.Router, m string, p string, hF http.HandlerFunc) {
	r.Methods(m).Path(p).Handler(hF)
}

func printRoutes(route *mux.Route, _ *mux.Router, _ []*mux.Route) error {
	pathTemplate, err := route.GetPathTemplate()
	methods, _ := route.GetMethods()
	if err == nil && len(methods) > 0 {
		log.Trace().Msgf("%s \t:: %s", methods, pathTemplate)
	}
	return nil
}
