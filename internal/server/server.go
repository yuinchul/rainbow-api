package server

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/rainbowproject/rainbow-api/internal/database"

	"gitlab.com/rainbowproject/rainbow-api/internal/config"

	"github.com/rs/zerolog/log"
)

func Start() {

	c := config.GetServerEnvVars()
	var (
		wait       = time.Second * 15
		serverAddr = fmt.Sprintf("API Address :: %s:%s", c.URI, c.Port)
		listenTo   = fmt.Sprintf("%s:%s", "0.0.0.0", c.Port)
		quitMsg    = "Server shutdown"
	)

	db := database.NewDBHandler().NewDBConnection()
	r := SetupRouter(db)

	srv := &http.Server{
		Addr:    listenTo,
		Handler: r,
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout:   time.Second * 10,
		ReadTimeout:    time.Second * 10,
		IdleTimeout:    time.Second * 60,
		MaxHeaderBytes: 1 << 20,
	}

	log.Info().Msg(serverAddr)

	go func() {
		err := srv.ListenAndServe()
		if err != nil {
			panic(err.Error())
		}
	}()

	q := make(chan os.Signal)
	signal.Notify(q, os.Interrupt)
	s := <-q

	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	_ = srv.Shutdown(ctx)

	log.Info().Msgf("%s :: %s", quitMsg, s)
	os.Exit(0)
}
