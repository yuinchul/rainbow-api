package messages

const (
	SqlPrepareFail = "Fail on prepare SQL statement"
	SqlFailExecute = "Fail on execute SQL query"
	SqlNotFound    = "Data not found"
)
