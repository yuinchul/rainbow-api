package controllers

import (
	"database/sql"
	"fmt"
	"gitlab.com/rainbowproject/rainbow-api/internal/database/doa"
	"gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
	"net/http"

	"github.com/rs/zerolog/log"
	"gitlab.com/rainbowproject/rainbow-api/internal/config"
	"gitlab.com/rainbowproject/rainbow-api/internal/messages"
	"gitlab.com/rainbowproject/rainbow-api/internal/services"
	"gitlab.com/rainbowproject/rainbow-api/internal/validator"
)

type ProfileController interface {
	FetchProfile(w http.ResponseWriter, r *http.Request)
	UpdateAvatar(w http.ResponseWriter, r *http.Request)
	UpdateProfile(w http.ResponseWriter, r *http.Request)
	Delete(w http.ResponseWriter, r *http.Request)
}

type profile struct {
	aDOA      doa.DAOAccount
	s         services.ProfileServiceHandler
	avatarCfg *config.AvatarConfig
}

func NewProfileController(db *sql.DB,
	v validator.ValidateJSON) *profile {
	return &profile{
		aDOA:      doa.NewDOAccountHandler(db),
		s:         services.NewProfileServiceHandler(db, v),
		avatarCfg: config.GetAvatarEnvVar(),
	}
}

// swagger:operation GET /v1/profile profile-api FetchProfile
//
// Fetch account profile
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     security:
//       - Bearer: []
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
func (p *profile) FetchProfile(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Get profile info request")
	email, ok := r.Context().Value("email").(string)
	if !ok {
		e := rest.NewInternalServerError(messages.BrokenToken)
		e.WriteResponse(w)
		return
	}
	account, uErr := p.aDOA.FetchByEmail(email)
	if uErr != nil {
		uErr.WriteResponse(w)
		return
	}
	s, e := p.s.FetchByAID(account.ID)
	if e != nil {
		e.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PATCH /v1/profile/avatar profile-api UpdateAvatar
//
// Update account profile avatar
//
// ---
//
//     consumes: [multipart/form-data]
//     produces: [application/json]
//
//     security:
//       - Bearer: []
//
//     parameters:
//     - name: avatar
//       in: formData
//       description: The uploaded avatar image data
//       required: true
//       type: file
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (p *profile) UpdateAvatar(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Update profile avatar request")
	email, ok := r.Context().Value("email").(string)
	if !ok {
		e := rest.NewInternalServerError(messages.BrokenToken)
		e.WriteResponse(w)
		return
	}
	account, uErr := p.aDOA.FetchByEmail(email)
	if uErr != nil {
		uErr.WriteResponse(w)
		return
	}
	pErr := r.ParseMultipartForm(p.avatarCfg.MaxSize << 20)
	if pErr != nil {
		msg := fmt.Sprintf("%s :: > %d mb",
			messages.ProfileAvatarMaxSize,
			p.avatarCfg.MaxSize)
		e := rest.NewBadRequestError(msg)
		log.Error().Msg(pErr.Error())
		e.WriteResponse(w)
		return
	}
	file, _, fErr := r.FormFile("avatar")
	if fErr != nil {
		e := rest.NewBadRequestError(messages.ProfileAvatarRetError)
		log.Error().Msg(fErr.Error())
		e.WriteResponse(w)
		return
	}
	defer file.Close()

	s, e := p.s.UpdateAvatar(account.ID, file)
	if e != nil {
		e.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation PATCH /v1/profile/info profile-api UpdateProfile
//
// Update account profile info
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     security:
//       - Bearer: []
//
//     parameters:
//     - name: body
//       in: body
//       description: profile info
//       required: true
//       schema:
//         $ref: "#/definitions/ProfileInfoReq"
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (p *profile) UpdateProfile(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Update profile info request")
	email, ok := r.Context().Value("email").(string)
	if !ok {
		e := rest.NewInternalServerError(messages.BrokenToken)
		e.WriteResponse(w)
		return
	}
	var input rest.ProfileInfoReq
	eV := decode(r.Body, &input)
	if eV != nil {
		eV.WriteResponse(w)
		return
	}
	account, uErr := p.aDOA.FetchByEmail(email)
	if uErr != nil {
		uErr.WriteResponse(w)
		return
	}
	s, e := p.s.UpdateProfile(account.ID, &input)
	if e != nil {
		e.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}

// swagger:operation DELETE /v1/profile profile-api UpdateProfile
//
// Delete account profile
//
// ---
//
//     consumes: [application/json]
//     produces: [application/json]
//
//     security:
//       - Bearer: []
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (p *profile) Delete(w http.ResponseWriter, r *http.Request) {
	log.Trace().Msg("CONTROLLER - Delete profile request")
	email, ok := r.Context().Value("email").(string)
	if !ok {
		e := rest.NewInternalServerError(messages.BrokenToken)
		e.WriteResponse(w)
		return
	}
	account, uErr := p.aDOA.FetchByEmail(email)
	if uErr != nil {
		uErr.WriteResponse(w)
		return
	}
	s, e := p.s.Delete(account.ID)
	if e != nil {
		e.WriteResponse(w)
		return
	}
	s.WriteResponse(w)
}
