package controllers

import (
	"gitlab.com/rainbowproject/rainbow-api/internal/models/rest"
	"net/http"

	"github.com/rs/zerolog/log"
)

type HealthController interface {
	HealthGet(http.ResponseWriter, *http.Request)
}

type health struct{}

func NewHealthController() *health {
	return &health{}
}

// swagger:operation GET /v1/health health-api Health
//
// Return an ok message if the service is up and running
//
// ---
//
//     produces: [application/json]
//
//     responses:
//       200:
//         description: Success Message
//         schema:
//             $ref: "#/definitions/Suc"
//
func (h *health) HealthGet(w http.ResponseWriter, _ *http.Request) {
	log.Trace().Msg("CONTROLLER - Health check request")
	ok := rest.NewBasicSuccess("ok", nil)
	ok.WriteResponse(w)
}
