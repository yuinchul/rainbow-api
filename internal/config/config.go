package config

import (
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

type Server struct {
	Name string
	URI  string
	Port string
}

type SMTP struct {
	Host string
	Port string
	User string
	Pass string
	From string
}

type Redis struct {
	Host string
	Port string
	Pass string
}

type DB struct {
	Username string
	Password string
	Host     string
	Schema   string
}

type JWT struct {
	Secret     string
	ExpireTime int
}

type AvatarConfig struct {
	MaxSize int64
}

func init() {
	viper.SetConfigFile(".env")
	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal().AnErr("Error while reading config file %s", err)
	}
}

func GetDebugLevelEnvVars() string {
	return viper.GetString("LOG_LEVEL")
}

func GetAppName() string {
	if !viper.IsSet("APP_NAME") {
		log.Fatal().Msg("APP_NAME env variable not defined")
	}
	return viper.GetString("APP_NAME")
}

func GetServerEnvVars() *Server {
	if !viper.IsSet("SERVER_NAME") {
		log.Fatal().Msg("SERVER_NAME env variable not defined")
	}
	if !viper.IsSet("SERVER_URI") {
		log.Fatal().Msg("SERVER_URI env variable not defined")
	}
	if !viper.IsSet("SERVER_PORT") {
		log.Fatal().Msg("SERVER_PORT env variable not defined")
	}
	return &Server{
		Name: viper.GetString("SERVER_NAME"),
		URI:  viper.GetString("SERVER_URI"),
		Port: viper.GetString("SERVER_PORT"),
	}
}

func GetSMTPEnvVars() *SMTP {
	if !viper.IsSet("SMTP_HOST") {
		log.Fatal().Msg("SMTP_HOST env variable not defined")
	}
	if !viper.IsSet("SMTP_PORT") {
		log.Fatal().Msg("SMTP_PORT env variable not defined")
	}
	if !viper.IsSet("SMTP_USER") {
		log.Fatal().Msg("SMTP_USER env variable not defined")
	}
	if !viper.IsSet("SMTP_PASS") {
		log.Fatal().Msg("SMTP_PASS env variable not defined")
	}
	if !viper.IsSet("SMTP_FROM") {
		log.Fatal().Msg("SMTP_FROM env variable not defined")
	}

	return &SMTP{
		Host: viper.GetString("SMTP_HOST"),
		Port: viper.GetString("SMTP_PORT"),
		User: viper.GetString("SMTP_USER"),
		Pass: viper.GetString("SMTP_PASS"),
		From: viper.GetString("SMTP_FROM"),
	}
}

func GetRedisEmailEvnVars() *Redis {
	if !viper.IsSet("REDIS_E_HOST") {
		log.Fatal().Msg("REDIS_E_HOST env variable not defined")
	}
	if !viper.IsSet("REDIS_E_PORT") {
		log.Fatal().Msg("REDIS_E_PORT env variable not defined")
	}
	if !viper.IsSet("REDIS_E_PASS") {
		log.Fatal().Msg("REDIS_E_PASS env variable not defined")
	}
	return &Redis{
		Host: viper.GetString("REDIS_E_HOST"),
		Port: viper.GetString("REDIS_E_PORT"),
		Pass: viper.GetString("REDIS_E_PASS"),
	}
}

func GetRedisPassEvnVars() *Redis {
	if !viper.IsSet("REDIS_P_HOST") {
		log.Fatal().Msg("REDIS_P_HOST env variable not defined")
	}
	if !viper.IsSet("REDIS_P_PORT") {
		log.Fatal().Msg("REDIS_P_PORT env variable not defined")
	}
	if !viper.IsSet("REDIS_P_PASS") {
		log.Fatal().Msg("REDIS_P_PASS env variable not defined")
	}
	return &Redis{
		Host: viper.GetString("REDIS_P_HOST"),
		Port: viper.GetString("REDIS_P_PORT"),
		Pass: viper.GetString("REDIS_P_PASS"),
	}
}

func GetDbEnvVars() *DB {
	if !viper.IsSet("DB_HOST") {
		log.Fatal().Msg("DB_HOST env variable is not defined")
	}
	if !viper.IsSet("DB_PORT") {
		log.Fatal().Msg("DB_PORT env variable is not defined")
	}
	if !viper.IsSet("DB_USER") {
		log.Fatal().Msg("DB_USER env variable is not defined")
	}
	if !viper.IsSet("DB_PASS") {
		log.Fatal().Msg("DB_PASS env variable is not defined")
	}
	if !viper.IsSet("DB_DB") {
		log.Fatal().Msg("DB_DB env variable is not defined")
	}
	return &DB{
		Host:     viper.GetString("DB_HOST"),
		Username: viper.GetString("DB_USER"),
		Password: viper.GetString("DB_PASS"),
		Schema:   viper.GetString("DB_DB"),
	}
}

func GetJWTEnvVar() *JWT {
	if !viper.IsSet("JWT_SECRET") {
		log.Fatal().Msg("JWT_SECRET env variable is not defined")
	}
	if !viper.IsSet("JWT_EXPIRE_MIN") {
		log.Fatal().Msg("JWT_EXPIRE_MIN env variable is not defined")
	}
	return &JWT{
		Secret:     viper.GetString("JWT_SECRET"),
		ExpireTime: viper.GetInt("JWT_EXPIRE_MIN"),
	}
}

func GetAvatarEnvVar() *AvatarConfig {
	if !viper.IsSet("AVATAR_MAX_SIZE") {
		log.Fatal().Msg("AVATAR_MAX_SIZE env variable is not defined")
	}
	return &AvatarConfig{
		MaxSize: viper.GetInt64("AVATAR_MAX_SIZE"),
	}
}
