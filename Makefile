BINDIR      := $(CURDIR)/bin
BINNAME     ?= rainbow

SRC         := $(sh find . -type f -name '*.go' -print)

COMMIT   := $(sh git rev-parse --short HEAD)
BUILT_AT := $(sh date +%FT%T%z)
BUILT_ON := $(sh hostname)

LDFLAGS := -w -s
LDFLAGS += -X main.commit=${COMMIT}
LDFLAGS += -X main.builtAt=${BUILT_AT}
LDFLAGS += -X main.builtBy=${USER}
LDFLAGS += -X main.builtOn=${BUILT_ON}

.PHONY: all
all: build

.PHONY: build
build: $(BINDIR)/$(BINNAME)
$(BINDIR)/$(BINNAME): $(SRC)
	go build -ldflags '$(LDFLAGS)' -o $(BINDIR)/$(BINNAME) ./cmd/rainbow/

.PHONY: check-go
check-go:
	go vet ./...
	#gocyclo -over 10 $(shell find . -iname '*.go' -type f | grep -v /vendor/)
	golint -set_exit_status $(shell go list ./... | grep -v mock)
	goimports -l $(shell find . -type f -name '*.go' -not -path "./vendor/*" -not -path "./packrd/*")

.PHONY: test-coverage
test-coverage:
	go test -v -coverprofile=coverage.txt -covermode=atomic ./...

.PHONY: clean
clean:
	rm -f ./bin/*

.PHONY: docker-compose-up
docker-compose-up:
	cd ./deployments && sudo docker-compose up -d

.PHONY: docker-compose-down
docker-compose-down:
	cd ./deployments && sudo docker-compose down

.PHONY: docker-build
docker-build:
	sudo docker build ./deployments -t tmendes/rainbow-users-api

.PHONY: openapi-update
openapi-update:
	swagger generate spec --scan-models -o api/openapi.json

.PHONY: openapi-run-server
openapi-run-server:
	swagger serve -F swagger ./api/openapi.json

all: build

